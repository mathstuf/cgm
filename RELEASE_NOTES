Version 14.1 (SIGMA v1.1)
=========================

This version of CGM is compatible with Cubit v14.0 (https://cubit.sandia.gov/public/14.0/Cubit-14.0-announcement.html) only.
The API updates for Cubit v14.1 will be available in the next minor release.

A. BUG FIXES
------------

 1. Fix a lot of warnings (over 700) appearing on Linux and OSX due to uninitialized variables, remove unsed variables etc. for CGM-13.1 and CGM-14.0.
 2. Trap unhandled exceptions more cleanly
 3. Fixed failing test cases on newer version of OSX
 4. Fixes for getting shared builds (Cubit/OCC) and make distcheck working cleanly in several configuration combinations

B. NEW FEATURES + ENHANCEMENTS
------------------------------

 1. Native support for building with both OpenCascade (OCC) and the community edition (OCE).
    Currently configured using --with-occ.
 2. A patched OCE fork on Bitbucket is managed by SIGMA team and can be used as is: https://bitbucket.org/fathomteam/oce.
    Using the fork of https://github.com/tpaviot/oce will require patching separately in order to work semalessly with CGM. 

      Version info:
      ------------
      OCC 6.7.1  <=>  OCE 0.16  <=>  Branch rajeeja/oce_0.16_occ_6.7.1
      OCC 6.6.0  <=>  OCE 0.13  <=>  Branch sigma/oce_0.13_occ_6.6
      OCC 6.5.3  <=>  OCE 0.10  <=>  Branch sigma/oce_0.10_occ_6.5.3

 3. Support for CMake-based builds
 4. Improved support for CGM build on Windows OS
 5. Improvements to documentation and added new tests

Version 14.0 (SIGMA v1.0)
=========================
See details on compatibility with dependent libraries in NEWS section of http://sigma.mcs.anl.gov

A. BUG FIXES
------------

 1. Fix stack smashing issues found on Intel builds while loading using function: iGeom_load_cub_geometry.
 2. Fix various warnings found on Ubuntu and OSX.
 3. Fixed the non-debug mode testcase 'modify' failure issue. Here the DLIList functions are not working as expected, and so rewrote the member accessing call.

B. NEW FEATURES
---------------

 1. CMake-based builds are now supported. Note that CMake scripts do not yet support parallel/MPI builds.
 2. Enhanced [Doxygen-based API documentation][http://ftp.mcs.anl.gov/pub/fathom/cgm-docs], users and developers guide for CGM.

C. API CHANGES
--------------

 1. iGeom_iface_correction

D. FUTURE WORK
--------------

 1. Fix OCC builds that trigger test failures
 2. Improve OCE support
 3. Improve compatibility with Cubit 14.1

